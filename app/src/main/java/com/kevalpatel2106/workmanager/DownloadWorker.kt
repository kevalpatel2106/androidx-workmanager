package com.kevalpatel2106.workmanager

import androidx.work.Worker

/**
 * Created by Kevalpatel2106 on 11-May-18.
 *
 * @author <a href="https://github.com/kevalpatel2106">kevalpatel2106</a>
 */
class DownloadWorker : Worker() {

    /**
     * This function will be called whenever the work manager run the work.
     */
    override fun doWork(): WorkerResult {

        Thread.sleep(5000)


        // Indicate success or failure with your return value.
        return WorkerResult.SUCCESS
    }
}
