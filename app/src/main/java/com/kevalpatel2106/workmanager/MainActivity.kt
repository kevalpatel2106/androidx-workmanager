package com.kevalpatel2106.workmanager

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import androidx.work.Constraints
import androidx.work.OneTimeWorkRequest
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        schedule_one_off_btn.setOnClickListener { scheduleForOneTime() }

    }

    private fun scheduleForOneTime() {
        val myConstraints = Constraints.Builder()
                .setRequiresDeviceIdle(true)
                .setRequiresCharging(true)
                .build()
        val request = PeriodicWorkRequest
                .Builder(DownloadWorker::class.java, 1, TimeUnit.HOURS)
                .setConstraints(myConstraints)
                .build()
        WorkManager.getInstance().enqueue(request)

        work_status_tv.text = "Work scheduled."

        WorkManager.getInstance()
                .getStatusById(request.id)
                .observe(this@MainActivity, Observer {
                    it?.let {
                        if (it.state.isFinished) {
                            Toast.makeText(this@MainActivity, "Work completed.", Toast.LENGTH_LONG).show()
                        }else{
                            Toast.makeText(this@MainActivity, "Work failed.", Toast.LENGTH_LONG).show()
                        }
                    }
                })
    }
}
